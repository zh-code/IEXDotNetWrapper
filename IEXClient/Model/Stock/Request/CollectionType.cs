﻿namespace IEXClient.Model.Stock.Request
{
    public enum CollectionType
    {
        Sector,
        Tag,
        List
    }
}