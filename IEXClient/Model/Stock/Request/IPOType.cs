﻿namespace IEXClient.Model.Stock.Request
{
    public enum IPOType
    {
        Upcoming,
        Today
    }
}