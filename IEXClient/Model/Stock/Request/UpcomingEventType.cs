﻿namespace IEXClient.Model.Stock.Request
{
    public enum UpcomingEventType
    {
        Events,
        Dividends,
        Splits,
        Earnings,
        IPOs
    }
}