﻿namespace IEXClient.Model.Stock.Request
{
    public enum BatchType
    {
        Quote,
        News,
        Chart
    }
}