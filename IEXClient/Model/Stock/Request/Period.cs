﻿namespace IEXClient.Model.Stock.Request
{
    public enum Period
    {
        Quarter,
        Annual
    }
}