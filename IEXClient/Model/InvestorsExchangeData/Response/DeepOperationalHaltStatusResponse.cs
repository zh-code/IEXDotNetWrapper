﻿namespace IEXClient.Model.InvestorsExchangeData.Response
{
    public class DeepOperationalHaltStatusResponse
    {
        public bool isHalted { get; set; }
        public long timestamp { get; set; }
    }
}