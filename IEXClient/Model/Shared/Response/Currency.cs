﻿namespace IEXClient.Model.Shared.Response
{
    public class Currency
    {
        public string code { get; set; }
        public string name { get; set; }
    }
}