﻿namespace IEXClient.Model.ReferenceData.Request
{
    public enum DirectionType
    {
        Next,
        Last
    }
}