﻿namespace IEXClient.Model.ReferenceData.Response
{
    public class InternationalExchangeResponse
    {
        public string exchange { get; set; }
        public string region { get; set; }
        public string description { get; set; }
    }
}