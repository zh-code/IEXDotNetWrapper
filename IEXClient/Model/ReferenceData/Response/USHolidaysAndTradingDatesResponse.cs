﻿using System;

namespace IEXClient.Model.ReferenceData.Response
{
    public class USHolidaysAndTradingDatesResponse
    {
        public DateTime date { get; set; }
        public DateTime settlementDate { get; set; }
    }
}